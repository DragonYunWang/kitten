import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from '../../http/request'
import { Pagination, Icon, List, WhiteSpace, WingBlank, Toast } from 'antd-mobile'
import Header from '../common/header'
import { HistroyrecordWrapper } from './style'
import { connect } from 'react-redux'
import config from '../../config/config'
import { actionCreators } from '../login/store'

const Item = List.Item

class Histroyrecord extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dataSource: [],
      total: 1,
      current: 1
    }
  }

  paginationChange = v => {
    this.setState(
      {
        current: v
      },
      () => {
        this.query()
      }
    )
  }

  componentDidMount() {
    this.query()
  }

  query = () => {
    const { current } = this.state
    const { uid } = this.props
    axios
      .get(config.baseURL + '/query', { params: { uid, pageSize: 10, offSet: (current - 1) * 10 } })
      .then(res => {
        const resultCode = res.data.code
        if (resultCode === 0) {
          this.setState({
            dataSource: res.data.msg.data,
            total: Math.ceil(res.data.msg.totalCount / 10)
          })
        } else if (resultCode === -1) {
          Toast.info('登录失效, 请重新登录 !!!', 1)
          this.props.logout()
        } else {
          Toast.fail(res.data.msg, 1)
        }
      })
  }

  render() {
    const { loginStatus } = this.props
    if (loginStatus) {
      const itemList = this.state.dataSource.map((item, index) => (
        <Item key={index}>
          <p>充值账户: {item.rechargeUid}</p>
          <p>充值数量: {item.rechargeAmount}</p>
          <p>充值时间: {item.rechargeTime}</p>
        </Item>
      ))
      return (
        <HistroyrecordWrapper>
          <Header>历史记录</Header>
          <WhiteSpace />
          {itemList.length > 0 ? <List>{itemList}</List> : '暂无记录'}
          <WhiteSpace />
          <WingBlank>
            <Pagination
              className="custom-pagination-with-icon"
              total={this.state.total}
              current={this.state.current}
              locale={{
                prevText: (
                  <span className="arrow-align">
                    <Icon type="left" />
                    上一页
                  </span>
                ),
                nextText: (
                  <span className="arrow-align">
                    下一步
                    <Icon type="right" />
                  </span>
                )
              }}
              onChange={this.paginationChange}
            />
          </WingBlank>
          <WhiteSpace />
        </HistroyrecordWrapper>
      )
    } else {
      console.log('logout')
      return <Redirect to="login" />
    }
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login']),
  uid: state.getIn(['login', 'uid'])
})

const mapDispatchToProps = dispatch => ({
  logout() {
    dispatch(actionCreators.logout())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Histroyrecord)
