import React, { Component, Fragment } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { WhiteSpace, List, InputItem, WingBlank, Button, Toast, Picker, SegmentedControl } from 'antd-mobile'
import axios from '../../http/request'
import { rechargeType, operateType } from '../../config/config'
import config from '../../config/config'
import Header from '../common/header'

class AgentSendCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataSource: [{ value: 26, label: '50元26张' }, { value: 55, label: '100元55张' }],
      rechargeUid: null,
      confirmRechargeUid: null,
      rechargeAmount: [26],
      rechargeType: rechargeType,
      operateType: operateType,
      gold: 0,
      diamond: 0,
      uName: '',
      type: 0,
      houseCard: [{ value: 26, label: '50元26张' }, { value: 55, label: '100元55张' }],
      catCard: [
        { value: 100000, label: '10万' },
        { value: 200000, label: '20万' },
        { value: 500000, label: '50万' },
        { value: 1000000, label: '100万' }
      ]
    }
  }
  rechargeUidChange = v => {
    console.log(v)
    this.setState({
      rechargeUid: v
    })
  }

  confirmRechargeUidChange = v => {
    this.setState({
      confirmRechargeUid: v
    })
  }

  getuName = () => {
    const { confirmRechargeUid, rechargeUid } = this.state
    if (confirmRechargeUid !== rechargeUid) {
      Toast.info('两次输入的代理商ID不一致,请重新输入')
      return
    }
    axios
      .get(config.baseURL + '/account/proxy', {
        params: {
          uid: this.state.confirmRechargeUid
        }
      })
      .then(res => {
        if (res.data.code === 0) {
          this.setState({
            uName: res.data.msg.uName
          })
        }
      })
  }

  handTypeChange = e => {
    const { houseCard, catCard } = this.state
    const type = e.nativeEvent.selectedSegmentIndex
    this.setState({
      type,
      dataSource: type === 0 ? houseCard : catCard,
      rechargeAmount: type === 0 ? [houseCard[0].value] : [catCard[0].value],
      rechargeType: type === 0 ? 0 : 1
    })
  }

  rechargeAmountChange = v => {
    this.setState({
      rechargeAmount: v
    })
  }

  recharge = () => {
    const { rechargeUid, rechargeAmount, rechargeType, operateType } = this.state
    axios
      .get(config.baseURL + '/recharge2b', {
        params: {
          rechargeUid,
          rechargeAmount: rechargeAmount[0],
          rechargeType,
          operateType
        }
      })
      .then(res => {
        if (res.data.code === 0) {
          Toast.success('充值成功')
          this.getGold()
        } else {
          Toast.success(res.data.msg)
        }
      })
  }
  render() {
    const { loginStatus } = this.props
    if (loginStatus) {
      const { gold, uName, rechargeUid, confirmRechargeUid, rechargeAmount, type, dataSource, diamond } = this.state
      const {
        rechargeUidChange,
        confirmRechargeUidChange,
        rechargeAmountChange,
        recharge,
        getuName,
        handTypeChange
      } = this
      return (
        <Fragment>
          <Header>发放房卡</Header>
          <WhiteSpace />
          <WingBlank>
            <SegmentedControl selectedIndex={type} values={['发放房卡', '发放猫卡']} onChange={handTypeChange} />
          </WingBlank>
          <WhiteSpace />
          <List>
            <InputItem type="number" placeholder="0" value={type=== 0? gold: diamond} disabled>
              当天剩余
            </InputItem>
            <InputItem type="number" placeholder="请输入代理商ID" onChange={rechargeUidChange} value={rechargeUid}>
              代理商ID
            </InputItem>
            <InputItem
              type="number"
              placeholder="请确认代理商ID"
              onChange={confirmRechargeUidChange}
              onBlur={getuName}
              value={confirmRechargeUid}
            >
              确认ID
            </InputItem>
            <InputItem type="text" disabled value={uName}>
              代理商昵称
            </InputItem>
            <Picker data={dataSource} cols={1} className="forss" onChange={rechargeAmountChange} value={rechargeAmount}>
              <List.Item arrow="horizontal">点击选择发放数量</List.Item>
            </Picker>
          </List>
          <WhiteSpace />
          <WingBlank>
            <Button type="primary" onClick={recharge}>
              发放
            </Button>
          </WingBlank>
        </Fragment>
      )
    } else {
      return <Redirect to="/login" />
    }
  }
  getGold = () => {
    const { uid } = this.props
    axios.get(config.baseURL + '/account/proxy', { params: { uid } }).then(res => {
      if (res.data.code === 0) {
        this.setState({
          gold: res.data.msg.gold,
          diamond: res.data.msg.diamond
        })
      }
    })
  }
  componentDidMount() {
    this.getGold()
  }
}
const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login']),
  uid: state.getIn(['login', 'uid'])
})
export default connect(
  mapStateToProps,
  null
)(AgentSendCard)
