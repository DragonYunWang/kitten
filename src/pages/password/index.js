import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import axios from '../../http/request'
import md5 from 'md5'
import { actionCreators as loginActionCreators } from '../login/store'
import { List, InputItem, Toast, WhiteSpace, WingBlank, Button } from 'antd-mobile'
import Header from '../common/header'
import config from '../../config/config'

class Pwd extends Component {
  constructor(props) {
    super(props)
    this.state = {
      oldPwd: '',
      newPwd: '',
      newPwdAngin: ''
    }
  }

  oldPwdChange = oldPwd => {
    this.setState({
      oldPwd
    })
  }

  newPwdChange = newPwd => {
    this.setState({
      newPwd
    })
  }

  newPwdAnginChange = newPwdAngin => {
    this.setState({
      newPwdAngin
    })
  }

  changePwd = (oldPwd, newPwd) => {
    if (newPwd !== this.state.newPwdAngin) {
      Toast.info('两次密码输入不一致!')
      return
    }

    axios
      .get(config.baseURL + '/modifyPwd', {
        params: {
          oldPwd: md5(oldPwd),
          newPwd
        }
      })
      .then(res => {
        const resultCode = res.data.code
        if (resultCode === 0) {
          Toast.success('修改成功, 请重新登录 !!!', 1)
          setTimeout(this.props.logout, 2000)
        } else if (resultCode === -1) {
          Toast.info('登录失效, 请重新登录 !!!', 1)
          this.props.logout()
        } else {
          Toast.fail('修改失败 !!!', 1)
        }
      })
  }

  render() {
    const { oldPwd, newPwd, newPwdAngin } = this.state
    const { loginStatus } = this.props

    if (loginStatus) {
      return (
        <div>
          <Header>修改密码</Header>
          <List renderHeader={() => '修改密码'}>
            <InputItem type="password" placeholder="请输入原登录密码" onChange={this.oldPwdChange} value={oldPwd} clear>
              原密码
            </InputItem>
            <InputItem type="password" placeholder="输入新登录密码" onChange={this.newPwdChange} value={newPwd} clear>
              新密码
            </InputItem>
            <InputItem
              type="password"
              placeholder="再次输入新登录密码"
              onChange={this.newPwdAnginChange}
              value={newPwdAngin}
              clear
            >
              确认密码
            </InputItem>
          </List>
          <WhiteSpace />
          <WingBlank>
            <Button type="primary" onClick={() => this.changePwd(oldPwd, newPwd)}>
              修改密码
            </Button>
          </WingBlank>
        </div>
      )
    } else {
      return <Redirect to="/login" />
    }
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login'])
})

const mapDispatchToProps = dispatch => ({
  logout() {
    dispatch(loginActionCreators.logout())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pwd)
