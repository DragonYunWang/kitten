import React, { Component, Fragment } from 'react'
import { Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Header from '../common/header'
import { List, WhiteSpace } from 'antd-mobile'
import { actionCreators } from './store'
const Item = List.Item

class Home extends Component {
  render() {
    const { loginStatus, uType } = this.props
    if (loginStatus) {
      return (
        <div>
          <Header isHomePage={true}>用户中心</Header>
          <WhiteSpace />
          <List>
            <Item
              thumb="https://zos.alipayobjects.com/rmsportal/UmbJMbWOejVOpxe.png"
              arrow="horizontal"
              onClick={() => {
                this.props.history.push('/changepwd')
              }}
            >
              修改密码
            </Item>
            <Item
              thumb="https://zos.alipayobjects.com/rmsportal/UmbJMbWOejVOpxe.png"
              arrow="horizontal"
              onClick={() => {
                this.props.history.push('/historyrecords')
              }}
            >
              历史记录
            </Item>
            <Item
              thumb="https://zos.alipayobjects.com/rmsportal/UmbJMbWOejVOpxe.png"
              arrow="horizontal"
              onClick={() => {
                this.props.history.push('/sendcard')
              }}
            >
              用户发卡
            </Item>
            {uType === 1 ? (
              <Fragment>
                <Item
                  thumb="https://zos.alipayobjects.com/rmsportal/UmbJMbWOejVOpxe.png"
                  arrow="horizontal"
                  onClick={() => {
                    this.props.history.push('/agentsendcard')
                  }}
                >
                  发卡
                </Item>
                <Item
                  thumb="https://zos.alipayobjects.com/rmsportal/UmbJMbWOejVOpxe.png"
                  arrow="horizontal"
                  onClick={() => {
                    this.props.history.push('/createuser')
                  }}
                >
                  创建用户
                </Item>
              </Fragment>
            ) : (
              ''
            )}

            <Item
              thumb="https://zos.alipayobjects.com/rmsportal/UmbJMbWOejVOpxe.png"
              arrow="horizontal"
              onClick={() => {
                this.props.history.push('/userecord')
              }}
            >
              消耗记录
            </Item>
          </List>
        </div>
      )
    } else {
      return <Redirect to="/login" />
    }
  }
  componentDidMount() {
    const { uid, getUserInfo } = this.props
    getUserInfo(uid)
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login']),
  uid: state.getIn(['login', 'uid']),
  uType: state.getIn(['home', 'uType'])
})

const mapDispatchToProps = dispatch => ({
  getUserInfo(uid) {
    dispatch(actionCreators.getUseInfo(uid))
  }
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home)
)
