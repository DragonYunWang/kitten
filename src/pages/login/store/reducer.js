import { fromJS } from 'immutable'
import * as constants from './constants'

const defaultState = fromJS({
  login: false,
  uid: null
})

export default (state = defaultState, action) => {
  switch (action.type) {
    case constants.CHANGE_LOGIN:
      return state.merge({ login: action.value, uid: action.uid })
    case constants.LOGOUT:
      return state.set('login', action.value)
    default:
      return state
  }
}
