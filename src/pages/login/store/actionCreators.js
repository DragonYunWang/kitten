import * as constants from './constants'
import axios from '../../../http/request'
import { Toast } from 'antd-mobile'
import config from '../../../config/config'

const changeLogin = uid => ({
  type: constants.CHANGE_LOGIN,
  value: true,
  uid
})

export const login = (uid, pwd, captcha) => {
  return dispatch =>
    axios.get(config.baseURL + '/login?uid=' + uid + '&pwd=' + pwd + '&captcha=' + captcha).then(res => {
      const resultCode = res.data.code
      if (resultCode === 0) {
        dispatch(changeLogin(uid))
      } else if (resultCode === 1) {
        Toast.info(res.data.msg)
      } else {
        Toast.info('登陆失败')
      }
    })
}

export const logout = () => ({
  type: constants.LOGOUT,
  value: false
})
