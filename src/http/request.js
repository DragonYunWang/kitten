/**
 * axios封装
 * 请求拦截、响应拦截、错误统一处理
 */
// import React from 'react'
// import { Redirect } from 'react-router-dom'
import store from '../store'
import axios from 'axios'
import config from '../config/config'
import { Toast } from 'antd-mobile'

import { actionCreators } from '../pages/login/store'

/**
 ** 提示函数
 * 禁止点击蒙层、显示一秒后关闭
 */
const tip = msg => {
  Toast.info(msg)
}

/**
 * 跳转登录页
 * 携带当前页面路由，以期在登录页面完成登录后返回当前页面
 */
// const toLogin = () => {
//   return <Redirect to="/login" />
// }

/**
 * 请求失败后的错误统一处理
 * @param {Number} status 请求失败的状态码
 */
// const errorHandle = (status, other) => {
//   // 状态码判断
//   switch (status) {
//     // 401: 未登录状态，跳转登录页
//     case 401:
//       tip('登录过期，请重新登录')
//       // 401 token过期
//       // 清除token并跳转登录页
//       store.commit(types.SET_LOGOUT)
//       setTimeout(() => {
//         toLogin()
//       }, 1000)
//       break
//     // 404请求不存在
//     case 404:
//       tip('请求的资源不存在')
//       break
//     default:
//       console.log(other)
//   }
// }

// 创建axios实例
const instance = axios.create({
  timeout: 1000 * 30,
  baseURL: config.baseUrl,
})

// 设置post请求头
instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
instance.defaults.withCredentials = true

/**
 * 请求拦截器
 * 每次请求前，如果存在token则在请求头中携带token
 */
// instance.interceptors.request.use(
//   // 在发送请求之前做些什么
//   config => {
//     // 遮蔽式请求框
//     loadingId = loading()
//     // 登录流程控制中，根据本地是否存在token判断用户的登录情况
//     // 但是即使token存在，也有可能token是过期的，所以在每次的请求头中携带token
//     // 后台根据携带的token判断用户的登录情况，并返回给我们对应的状态码
//     // 而后我们可以在响应拦截器中，根据状态码进行一些统一的操作。
//     const token = store.state.token
//     token && (config.headers.Authorization = token)
//     return config
//   },
//   error => {
//     loadingId.close()
//     // 对请求错误做些什么
//     return Promise.reject(error)
//   }
// )

// 响应拦截器
instance.interceptors.response.use(
  // 请求成功
  res => {
    if (res.data.code === -1) {
      tip('登录过期，请重新登录')
      store.dispatch(actionCreators.logout())
    }
    return res.status === 200 ? Promise.resolve(res) : Promise.reject(res)
  }
  // 请求已发出，但是不在2xx的范围
  // error => {
  //   const { response } = error
  //   // 对响应错误做点什么
  //   if (response) {
  //     errorHandle(response.status)
  //     return Promise.reject(error)
  //   }
  // }
)

export default instance
