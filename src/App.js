import React, { Component, Fragment } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { Provider } from 'react-redux'
import { GlobalStyle } from './style'
import store from './store'
import Home from './pages/home'
import Login from './pages/login'
import Pwd from './pages/password'
import Historyrecord from './pages/historyrecord'
import UserCard from './pages/usecard'
import SendCard from './pages/sendcard'
import AgentSendCard from './pages/agentsendcard'
import CreateUser from './pages/createuser'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Fragment>
          <GlobalStyle />
          <BrowserRouter>
            <div>
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/home" exact component={Home} />
                <Route path="/login" exact component={Login} />
                <Route path="/changepwd" exact component={Pwd} />
                <Route path="/historyrecords" exact component={Historyrecord} />
                <Route path="/sendcard" exact component={SendCard} />
                <Route path="/agentsendcard" exact component={AgentSendCard} />
                <Route path="/createuser" exact component={CreateUser} />
                <Route path="/userecord" exact component={UserCard} />
                <Redirect to="/" />
              </Switch>
            </div>
          </BrowserRouter>
        </Fragment>
      </Provider>
    )
  }
}

export default App
